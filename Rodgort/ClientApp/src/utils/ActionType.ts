export enum ActionType {
    REMOVED_TAG = 1,
    ADDED_TAG = 2,
    CLOSED = 3,
    REOPENED = 4,
    DELETED = 5,
    UNDELETED = 6
}