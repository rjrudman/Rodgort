import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { first, switchMap } from 'rxjs/operators'


@Injectable()
export class HttpAuthenticationInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return this.authService.GetAuthDetails().pipe(first()).pipe(switchMap(d => {
            const modified =
                d.IsAuthenticated
                    ? request.clone({ setHeaders: { 'Authorization': `Bearer ${d.RawToken}` } })
                    : request.clone();

            return next.handle(modified);
        }));
    }
}
