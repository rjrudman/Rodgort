import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActionType } from '../../utils/ActionType';

@Component({
  selector: 'app-unknown-deletion-resolution',
  templateUrl: './unknown-deletion-resolution.component.html',
  styleUrls: ['./unknown-deletion-resolution.component.scss']
})
export class UnknownDeletionResolutionComponent implements OnInit {
  public getRevision: (postId: number) => Promise<XMLHttpRequest>;

  public postsToVisit = [];

  public dryRun = true;

  private userIdRegex = /\/users\/(\-?\d+)/g;
  private deletedUserRegex = /by user(\d+)/g;

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    const checkerInterval = setInterval(() => {
      const untypedWindow = window as any;
      if (untypedWindow.getRevision) {
        this.getRevision = untypedWindow.getRevision;
        clearInterval(checkerInterval);

        this.Setup();
      }
    }, 500);
  }

  private Setup() {
    this.httpClient.get('/api/admin/UnresolvedDeletions')
      .subscribe((data: any) => {
        this.postsToVisit = data.map(action => ({
          unknownDeletionId: action.unknownDeletionId,
          postId: action.postId,
          info: '',
          status: 'pending'
        }));
      });
  }

  private GetActions(container: HTMLElement, unknownDeletionId: number) {
    let requests = [];
    const revisions = Array.from(container.querySelectorAll('.js-revision'));
    revisions.forEach(element => {
      const revisionDateInfo = element.querySelector('.relativetime');
      const date = revisionDateInfo.getAttribute('title');

      if (!!element.querySelector('.s-expandable')) {
        // We've got a diff, so we need to check for tags

        const userInfo = element.querySelector('.s-user-card > a');
        // Could be a deleted user
        if (!userInfo) {
          return;
        }

        const match = userInfo.getAttribute('href').match(/\/users\/(\-?\d+)/);
        const userId = parseInt(match[1], 10);

        const postTags = Array.from(element.querySelectorAll('.post-tag'))
          .filter(pt => !!pt.querySelector('span'));

        if (postTags.length <= 0) {
          return;
        }

        requests = requests.concat(postTags.map(e => {
          return {
            unknownDeletionId: unknownDeletionId,
            userId,
            tag: e.querySelector('span').innerHTML,
            actionTypeId: !!e.querySelector('span.diff-delete') ? ActionType.REMOVED_TAG : ActionType.ADDED_TAG,
            dateTime: date
          };
        }));
      } else {
        // No diff, so we check for votes
        const revisionVoteInfo = element.querySelector('div.js-revision > div > div:nth-child(2)');
        const xml = revisionVoteInfo.innerHTML;

        let actionTypeId = -1;
        if (xml.indexOf('<b>Post Closed</b>') > 0) {
          actionTypeId = ActionType.CLOSED;
        } else if (xml.indexOf('<b>Post Reopened</b>') > 0) {
          actionTypeId = ActionType.REOPENED;
        } else if (xml.indexOf('<b>Post Deleted</b>') > 0) {
          actionTypeId = ActionType.DELETED;
        } else if (xml.indexOf('<b>Post Undeleted</b>') > 0) {
          actionTypeId = ActionType.UNDELETED;
        }
        if (actionTypeId === -1) {
          return;
        }

        let userIdMatch= this.userIdRegex.exec(xml);
        if (!userIdMatch) {
          userIdMatch = this.deletedUserRegex.exec(xml);
        }
        while (userIdMatch != null) {
          const userId = parseInt(userIdMatch[1], 10);
          requests.push({
            unknownDeletionId: unknownDeletionId,
            userId,
            actionTypeId,
            dateTime: date
          });
          userIdMatch = this.userIdRegex.exec(xml);
        }
      }
    });
    return requests;
  }

  public startProcessing() {
    let i = 0;
    const TIMEOUT_DURATION = 1000
    const processNext = () => {
      if (i > this.postsToVisit.length - 1) {
        return;
      }

      const postToVisit = this.postsToVisit[i++];
      postToVisit.status = 'processing';
      const revisionInfoPromise = this.getRevision(postToVisit.postId);
      revisionInfoPromise.then(revisionInfo => {
        const container = document.implementation.createHTMLDocument('').documentElement;
        container.innerHTML = revisionInfo.responseText;

        const actions = this.GetActions(container, postToVisit.unknownDeletionId);

        if (actions.length) {
          if (this.dryRun) {
            console.log(actions);
            postToVisit.status = 'done';
            setTimeout(processNext, TIMEOUT_DURATION);
          } else {
            this.httpClient.post('/api/admin/ResolveUnresolvedDeletion', actions)
              .subscribe(_ => {
                postToVisit.status = 'done';
                setTimeout(processNext, TIMEOUT_DURATION);
              }, _ => {
                postToVisit.status = 'failed'
                setTimeout(processNext, TIMEOUT_DURATION);
              });
          }
        } else {
          console.error('Could not find any revisions for post ' + postToVisit.postId);
          postToVisit.status = 'failed';
          setTimeout(processNext, TIMEOUT_DURATION);
        }
      });
    };
    processNext();
  }
}
