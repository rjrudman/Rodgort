﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rodgort.Migrations
{
    public partial class UnmarkUnknownDeletionsWithNoAction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE unknown_deletions
SET processed_by_user_id = null, processed = null
FROM unknown_deletions ud
left join user_actions on ud.id = unknown_deletion_id
where user_actions.id is null
and unknown_deletions.id = ud.id
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
