﻿using System.IO;
using System.Net;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace StackExchangeChat.Console
{
    class Program
    {
        public static void Main(string[] args)
        {
            System.Console.WriteLine("Hello World!");

            var serviceCollection = new ServiceCollection();
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true)
                .AddJsonFile("appsettings.dev.json", true);

            //apiThing.TotalQuestionsByTag("stackoverflow", "design").GetAwaiter().GetResult();
            //apiThing.TotalQuestionsByTag("stackoverflow", "design").GetAwaiter().GetResult();
            //apiThing.TotalQuestionsByTag("stackoverflow", "design").GetAwaiter().GetResult();

            //var result = apiThing.QuestionsByTag("meta.stackoverflow", "burninate-request").GetAwaiter().GetResult();

            //ApiClient.QuotaRemaining.Subscribe(System.Console.WriteLine);

            // var result = apiThing.TotalQuestionsByTag("design").GetAwaiter().GetResult();


            // newBurninationService.CreateRoomForBurn(ChatSite.StackOverflow, ChatRooms.SO_BOTICS_WORKSHOP, "priority", "https://meta.stackoverflow.com/questions/285084/should-we-burninate-the-priority-tag").GetAwaiter().GetResult();
            // var roomId = chatClient.CreateRoom(ChatSite.StackOverflow, 167908, "This is a testing room", "This is a testing description").GetAwaiter().GetResult();
            //var events = chatClient.SubscribeToEvents(ChatSite.StackExchange, 86421);
            //events.Subscribe(System.Console.WriteLine);

            //chatClient
            //    .SubscribeToEvents(ChatSite.StackExchange, 86421)
            //    .OnlyMessages()
            //    .SameRoomOnly()
            //    .SkipMyMessages()
            //    .Subscribe(async chatEvent =>
            //    {
            //        try
            //        {
            //            await chatClient.SendMessage(chatEvent.RoomDetails.ChatSite, chatEvent.RoomDetails.RoomId, $":{chatEvent.ChatEventDetails.MessageId} Replying to message..");
            //        }
            //        catch (Exception) { }
            //    }, exception =>
            //    {
            //        System.Console.WriteLine(exception);
            //    });

            System.Console.ReadKey();
        }
    }
}
